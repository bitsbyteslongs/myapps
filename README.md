# README #



### What is this repository for? ###

* This app combines the latest machine learning technology and the mobile development to convert a smartphone into a smart ear for 
* people with impaired hearing to detect sounds in the surrounding area for any potential danger.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* The source code contains all the information you need to set up a Xcode project on your own.
* You will need a smartphone or a laptop with a microphone to test the application.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact